package app

import io.restassured.RestAssured
import io.restassured.RestAssured.get
import org.hamcrest.Matchers.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test


class ApiTest {

    @BeforeEach
    fun setup() {
        RestAssured.baseURI = System.getProperty("url")
        RestAssured.port = 80
    }
    
    @Test
    fun `test GET users API endpoint`() {
        get("/users")
            .then()
                .statusCode(200).assertThat()
                .body("size()", equalTo(2))
                .and()
                .body("[0].id", not(isEmptyString()))
                .and()
                .body("[0].email", not(isEmptyString()))
                .and()
                .body("[0].firstname", not(isEmptyString()))
                .and()
                .body("[0].lastname", not(isEmptyString()))
                .and()
                .body("[0].id", equalTo(2))
                .and()
                .body("[0].email", equalTo("elon_musk@test.test"))
                .and()
                .body("[0].firstname", equalTo("Elon"))
                .and()
                .body("[0].lastname", equalTo("Musk"))
    }
}
