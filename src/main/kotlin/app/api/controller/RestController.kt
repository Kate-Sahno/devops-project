package app.api.controller

import app.storage.entity.UserEntity
import app.storage.repository.UserJpaRepository
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.ws.rs.Consumes
import javax.ws.rs.Produces

@RestController
@RequestMapping("/")
@Produces(MediaType.APPLICATION_JSON_UTF8_VALUE)
@Consumes(MediaType.APPLICATION_JSON_UTF8_VALUE)
class MainController(val repository: UserJpaRepository) {
    @GetMapping("users")
    fun getAllUsers(): MutableList<UserEntity?> = repository.findAll()
}
