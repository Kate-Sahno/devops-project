package app.storage.entity

import javax.persistence.*

@Entity
@Table(name = "users")
class UserEntity {
    
    @get:Id
    @get:Basic
    @get:GeneratedValue(strategy = GenerationType.IDENTITY)
    @get:Column(name = "id", nullable = false, insertable = false, updatable = false)
    var id: Long? = null
    
    @get:Basic
    @get:Column(name = "email", nullable = false)
    var email: String? = null
    
    @get:Basic
    @get:Column(name = "firstname", nullable = true)
    var firstname: String? = null
    
    @get:Basic
    @get:Column(name = "lastname", nullable = true)
    var lastname: String? = null
    
    @get:Basic
    @get:Column(name = "male", nullable = true)
    var male: Boolean? = null
    
    override fun toString(): String =
            "Entity of type: ${javaClass.name} ( " +
                    "id = $id " +
                    "email = $email " +
                    "firstname = $firstname " +
                    "lastname = $lastname " +
                    ")"

    // constant value returned to avoid entity inequality to itself before and after it's update/merge
    override fun hashCode(): Int = 42

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as UserEntity

        if (id != other.id) return false
        if (email != other.email) return false
        if (firstname != other.firstname) return false
        if (lastname != other.lastname) return false

        return true
    }
}
