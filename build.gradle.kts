import org.gradle.api.tasks.testing.logging.TestLogEvent
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.run.BootRun

apply(plugin = "io.spring.dependency-management")
apply(plugin = "java")

buildscript {
    repositories {
        gradlePluginPortal()
        mavenCentral()
        jcenter()
    }
    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.30")
        classpath("org.jetbrains.kotlin:kotlin-allopen:1.3.30")
        classpath("org.jetbrains.kotlin:kotlin-noarg:1.3.30")
        classpath("javax.annotation:javax.annotation-api:1.3.2")
        classpath("javax.activation:activation:1.1.1")
        classpath("javax.transaction:javax.transaction-api:1.3")
        classpath("javax.xml.bind:jaxb-api:2.3.1")
        classpath("com.sun.xml.bind:jaxb-core:2.3.0.1")
        classpath("com.sun.xml.bind:jaxb-impl:2.3.2")
        classpath("com.sun.xml.ws:jaxws-ri:2.3.2")
    }
}

plugins {
    "base"
    "java"
    "kotlin"
    "kotlin-spring"
    "kotlin-jpa"
    "kotlin-allopen"
    "org.springframework.boot"
    id("org.springframework.boot") version "2.1.5.RELEASE"
    kotlin("jvm") version "1.3.30"
    kotlin("plugin.spring") version "1.3.30"
    kotlin("plugin.jpa") version "1.3.30"
    id("com.dorongold.task-tree") version "1.5"
}

allOpen {
    annotation("javax.persistence.Entity")
    annotation("javax.persistence.MappedSuperclass")
    annotation("javax.persistence.Embeddable")
}

java.sourceCompatibility = JavaVersion.VERSION_12
java.targetCompatibility = JavaVersion.VERSION_12

repositories {
    maven(url = "https://repo.spring.io/snapshot")
    maven(url = "https://repo.spring.io/milestone")
    mavenCentral()
    jcenter()
    flatDir{ dirs("libs") }
}

val kotlinVersion = "1.3.30"
val springBootVersion = "2.1.5.RELEASE"
val junitVersion = "5.4.2"

sourceSets {
    create("apiTest") {
        compileClasspath += fileTree("${rootDir}/apiTest/kotlin")
        compileClasspath += fileTree("src/apiTest/kotlin")
    }
}

val apiTestImplementation by configurations.getting {
    extendsFrom(configurations.testImplementation.get())
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${kotlinVersion}")
    implementation("org.jetbrains.kotlin:kotlin-reflect:${kotlinVersion}")

    implementation("javax.xml.bind:jaxb-api:2.3.1")
    implementation("javax.ws.rs:javax.ws.rs-api:2.0")
    implementation("javax.annotation:javax.annotation-api:1.3.2")
    implementation("javax.activation:activation:1.1.1")
    implementation("javax.transaction:javax.transaction-api:1.3")
    implementation("javax.xml.bind:jaxb-api:2.3.1")
    implementation("com.sun.xml.bind:jaxb-core:2.3.0.1")
    implementation("com.sun.xml.bind:jaxb-impl:2.3.2")
    implementation("com.sun.xml.ws:jaxws-ri:2.3.2")

    implementation("org.springframework.ws:spring-ws-core")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-jdbc")
    implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa") {
        exclude(group = "org.apache.tomcat", module = "tomcat-jdbc")
    }
    implementation("org.flywaydb:flyway-core")
    implementation("mysql:mysql-connector-java:6.0.5")
    implementation("org.hibernate:hibernate-core:5.4.10.Final")
    implementation("org.hibernate:hibernate-java8:5.4.10.Final")
    implementation("org.apache.commons:commons-lang3:3.9")
    implementation("commons-codec:commons-codec:1.13")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.6")
    implementation("org.webjars:bootstrap:4.4.1")
    implementation("org.webjars:jquery:3.4.1")

    testImplementation("org.junit.jupiter:junit-jupiter:${junitVersion}")
    testImplementation("org.junit.jupiter:junit-jupiter-api:${junitVersion}")
    testImplementation("org.junit.jupiter:junit-jupiter-params:${junitVersion}")
    testImplementation("com.nhaarman:mockito-kotlin:1.6.0")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5:${kotlinVersion}")
    testImplementation("com.h2database:h2:1.4.197")
    testImplementation("com.github.tomakehurst:wiremock-jre8:2.25.0")
    testImplementation("org.assertj:assertj-core-java8:1.0.0m1") {
        exclude(module = "junit")
    }
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(module = "junit")
    }

    apiTestImplementation("io.rest-assured:rest-assured:3.0.0")
    apiTestImplementation("org.hamcrest:hamcrest-all:1.3")

    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
}

tasks.named<KotlinCompile>("compileKotlin") {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "12"
    }
}

tasks.named<KotlinCompile>("compileTestKotlin") {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "12"
    }
    source += sourceSets.named("apiTest").get().output.asFileTree
}

tasks.named<Test>("test") {
    testLogging {
        events = setOf(
                TestLogEvent.PASSED,
                TestLogEvent.SKIPPED,
                TestLogEvent.FAILED,
                TestLogEvent.STANDARD_ERROR
        )
    }
    useJUnitPlatform()
}

tasks.named<BootRun>("bootRun") {
    main = "app.AppKt"
}

val integrationTest = task<Test>("apiTest") {
    description = "Runs api tests."
    group = "verification"
    
    
    testClassesDirs = sourceSets["apiTest"].output.classesDirs
    classpath = sourceSets["apiTest"].runtimeClasspath
    
    testLogging {
        events = setOf(
                TestLogEvent.PASSED,
                TestLogEvent.SKIPPED,
                TestLogEvent.FAILED,
                TestLogEvent.STANDARD_ERROR
        )
    }

    useJUnitPlatform()
    
    systemProperty("url", System.getProperty("url"))

    outputs.upToDateWhen { false }
}

tasks.named<Delete>("clean") {
    delete("out/")
}
