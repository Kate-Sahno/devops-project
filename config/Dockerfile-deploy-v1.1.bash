FROM python:3.9-rc-alpine3.10
RUN pip3 install awscli --upgrade \
  && apk add --update curl jq \
  && apk add bash \
  && curl -o /usr/local/bin/ecs-cli https://amazon-ecs-cli.s3.amazonaws.com/ecs-cli-linux-amd64-latest \
  && chmod +x /usr/local/bin/ecs-cli \
  && apk del curl \
  && rm -rf /var/cache/apk/*
CMD "/bin/bash"