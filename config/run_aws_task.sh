#!/bin/bash

#variable
BUILD_NUMBER=$1
TASK_DEFINITION_FILE=$2
CLUSTER=$3
DB_URL=$4
DB_USER=$5
DB_PWD=$6
REPOSITORY=$7
KEY_REPOSYTORY=$8


#  Checking and stopping old tasks
  TASK_ID=$(aws ecs list-tasks --cluster $CLUSTER | jq '.taskArns[]' | awk  -F '/' '{$1=""; print $0}' | awk  -F "\"" '{print $1}')
  if [ -z $TASK_ID ]
  then 
   echo "No running tasks."
  else
   echo "Stopping old tasks."
   aws ecs stop-task --task $TASK_ID --cluster $CLUSTER > /dev/null
  fi

#   Making AWS ECS TaskDefinition
touch task_definition_bak.json 
cat $TASK_DEFINITION_FILE | \
jq ".containerDefinitions[0].image = \"$REPOSITORY:$BUILD_NUMBER\"" | \
jq ".containerDefinitions[0].repositoryCredentials.credentialsParameter = \"$KEY_REPOSYTORY\"" | \
jq ".containerDefinitions[0].logConfiguration.options.\"awslogs-group\" = \"$CLUSTER\"" | \
jq ".containerDefinitions[0].name = \"kate-project-app-$BUILD_NUMBER\"" | \
jq ".containerDefinitions[0].environment[0].value = \"$DB_URL\"" | \
jq ".containerDefinitions[0].secrets[0].valueFrom = \"$DB_USER\"" | \
jq ".family = \"$CLUSTER\"" | \
jq ".containerDefinitions[0].secrets[1].valueFrom = \"$DB_PWD\"" >> task_definition_bak.json 

#   Registering new AWS Task using TaskDefinition
AWS_REGISTER_DEFINITION=$(aws ecs register-task-definition --cli-input-json file://task_definition_bak.json | jq '.taskDefinition.taskDefinitionArn')

#   Starting alredy registered AWS ECS Task
RUN_TASK_COMMAND="aws ecs run-task --task-definition $AWS_REGISTER_DEFINITION --cluster $CLUSTER"
eval $RUN_TASK_COMMAND > /dev/null && echo "New task is running."
rm -f ./task_definition_bak.json
